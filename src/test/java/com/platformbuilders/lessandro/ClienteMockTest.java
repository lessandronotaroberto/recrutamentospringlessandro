package com.platformbuilders.lessandro;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.platformbuilders.dto.ClienteDto;
import com.platformbuilders.model.Cliente;
import com.platformbuilders.repository.ClienteRepository;
import com.platformbuilders.resources.exception.ValidationException;
import com.platformbuilders.service.ClienteService;

@RunWith(SpringRunner.class)
public class ClienteMockTest {

	@InjectMocks
	private ClienteService clienteService;

	@Mock
	private ClienteRepository clienteRepository;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insereCliente() {
		try {
			ClienteDto clienteDto = new ClienteDto("Marco Pereira", "12345689563", "10/02/1975");
			Cliente cliente = clienteService.preparaNovoCliente(clienteDto);
			when(clienteRepository.save(any(Cliente.class))).thenReturn(cliente);
			assertNotNull(cliente.getCpf());
		} catch (ValidationException e) {
			Assert.fail();
		}
	}

}
