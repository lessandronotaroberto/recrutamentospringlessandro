package com.platformbuilders.service;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platformbuilders.model.Cliente;
import com.platformbuilders.repository.ClienteRepository;

@Service
public class DBService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public void instantiateTestDatabase() throws ParseException {
		
		Cliente cli1 = new Cliente(null, "Joaquim da Silva", "12345678978", new Date(1980, 12, 10));
		Cliente cli2 = new Cliente(null, "Maria Peixoto", "32456978451", new Date(1980, 12, 10));
		Cliente cli3 = new Cliente(null, "Pedro Tavares", "98725632145", new Date(1980, 12, 10));
		clienteRepository.saveAll(Arrays.asList(cli1, cli2, cli3));
	}
}
