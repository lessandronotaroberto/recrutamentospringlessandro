package com.platformbuilders.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.platformbuilders.dto.ClienteDto;
import com.platformbuilders.model.Cliente;
import com.platformbuilders.repository.ClienteRepository;
import com.platformbuilders.resources.exception.ValidationException;
import com.platformbuilders.service.exceptions.DataIntegrityException;
import com.platformbuilders.service.exceptions.ObjectNotFoundException;
import com.platformbuilders.util.DataUtil;
import com.platformbuilders.util.NumberUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Service
@Api(value = "Classe de serviço contendo as regras de negócio e/ou controle do cliente")
public class ClienteService {

	@Autowired
	private ClienteRepository repo;

	/**
	 * Recupera o cliente pelo identificador
	 * @param id Identificador
	 * @return Cliente
	 */
	@ApiOperation(value = "Recupera o cliente pelo número identificador")
	public Cliente find(Long id) {
		Optional<Cliente> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}
	
	/**
	 * Recupera o cliente pelo cpf
	 * @param id Identificador
	 * @return Cliente
	 */
	@ApiOperation(value = "Recupera o cliente pelo cpf")
	public Cliente buscaPorCpf(String cpf) {
		Cliente cliente = repo.buscaPorCpf(cpf);
		return cliente;
	}
	
	/**
	 * Insere o histórico
	 * @param clienteDto Cliente
	 * @return Cliente persistido.
	 * @throws ValidationException 
	 */
	@ApiOperation(value = "Insere na tabela de cliente as informações do cliente")
	public Cliente insert(ClienteDto clienteDto) throws ValidationException {
		Cliente cliente = preparaNovoCliente(clienteDto);
		return salvaCliente(cliente);
	}
	
	/**
	 * Prepara o Cliente para ser persistido no banco de dados.
	 * @param clienteDTO Objeto cliente preenchido à partir do JSON
	 * @return Entidade Cliente 
	 * @throws ValidationException 
	 */
	@ApiOperation(value = "Prepara o objeto Cliente através das informações de input")
	public Cliente preparaNovoCliente(ClienteDto clienteDto) throws ValidationException {
		if (!StringUtils.isEmpty(clienteDto.getDataNascimento())) {
			return new Cliente(null, clienteDto.getNome(), clienteDto.getCpf(), DataUtil.converteStringDataHora(clienteDto.getDataNascimento()));
		} 
		return new Cliente(clienteDto.getNome(), clienteDto.getCpf());
	}
	
	/**
	 * Prepara o Cliente para ser persistido no banco de dados.
	 * @param clienteDTO Objeto cliente preenchido à partir do JSON
	 * @return Entidade Cliente 
	 * @throws ValidationException 
	 */
	@ApiOperation(value = "Prepara o objeto Cliente através das informações de input")
	public Cliente preparaEdicaoCliente(ClienteDto clienteDto, Long id) throws ValidationException {
		if (!StringUtils.isEmpty(clienteDto.getDataNascimento())) {
			return new Cliente(id, clienteDto.getNome(), clienteDto.getCpf(), DataUtil.converteStringDataHora(clienteDto.getDataNascimento()));
		} 
		return new Cliente(clienteDto.getNome(), clienteDto.getCpf());
	}

	/**
	 * Grava o cliente.
	 * @param cliente Cliente preenchido.
	 * @return Cliente persistido.
	 */
	@ApiOperation(value = "Grava o objeto Cliente através das informações de input")
	public Cliente salvaCliente(Cliente cliente) {
		cliente = repo.save(cliente);
		if (cliente.getId()  != null)
			return cliente;
		throw new ObjectNotFoundException("Ocorreu um erro durante a gravação do cliente");
	}

	/**
	 * Remove o cliente por Id.
	 * @param id Identificador
	 */
	@ApiOperation(value = "Remove o cliente pelo identificador")
	public void delete(Long id) {
		find(id);
		try {
			repo.deleteById(id);
		}
		catch (DataIntegrityException e) {
			throw new DataIntegrityException("Não é possível excluir o cliente.");
		}
	}
	
	/**
	 * Retorna todos os clientes
	 * @return Clientes
	 */
	@ApiOperation(value = "Recupera todos os clientes")
	public List<Cliente> findAll() {
		return repo.findAll();
	}
	
	/**
	 * Retorna os clientes de forma paginada.
	 * @param page Página
	 * @param linesPerPage Registros por página
	 * @param orderBy Campo ordenado
	 * @param direction Tipo de ordenação
	 * @return Clientes paginados
	 */
	@ApiOperation(value = "Recupera os clientes de forma paginada")
	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Page<Cliente> historicos = repo.findAll(pageRequest);
		if (!historicos.isEmpty())
			return historicos;
		throw new ObjectNotFoundException("Nenhum cliente foi encontrado.");
	}
	
	/**
	 * Prepara o objeto DTO à partir da entidade para exibição no formato JSON.
	 * @param cliente Cliente do banco
	 * @return Cliente para exibição no formato JSON
	 * @throws ValidationException
	 */
	@ApiOperation(value = "Prepara os dados de retorno do Cliente para ser exibido em formato JSON")
	public ClienteDto preparaClienteDto(Cliente cliente) throws ValidationException {
		if (cliente.getDataNascimento() != null) {
			String dataNascimento = DataUtil.converteDataHoraString(cliente.getDataNascimento());
			int idade = DataUtil.calculaIdade(DataUtil.converteDateLocalDate(cliente.getDataNascimento()), DataUtil.converteDateLocalDate(new Date()));
			return new ClienteDto(cliente.getNome(), NumberUtil.formataCpf(cliente.getCpf()), dataNascimento, idade);
		}
		return new ClienteDto(cliente.getNome(), cliente.getCpf());
	}

	public ClienteDto buscaPorNomeCpf(ClienteDto clienteDto) throws ValidationException {
		Cliente cliente = repo.buscaPorNomeCpf(clienteDto.getNome() != null ? clienteDto.getNome().toLowerCase() : null, 
				clienteDto.getCpf());
		ClienteDto clienteDtoDB = preparaClienteDto(cliente);
		return clienteDtoDB;
	}

	public Boolean removePorCpf(String cpf) {
		try {
			Cliente cliente = buscaPorCpf(cpf);
			if (cliente != null) {
				repo.delete(cliente);
				return Boolean.TRUE;
			}
			throw new ObjectNotFoundException("O cliente não foi encontrado com esse cpf.");
		} catch (DataIntegrityException e) {
			throw new DataIntegrityException("Não é possível excluir o cliente.");
		}
	}

	public Cliente atualizarCliente(ClienteDto clienteDto, String id) throws ValidationException {
		if (StringUtils.isNotEmpty(id)) {
			Optional<Cliente> clienteDB = repo.findById(Long.parseLong(id));
			if (clienteDB != null && clienteDB.isPresent()) {
				Cliente cliente = clienteDB.get();
				cliente.setCpf(clienteDto.getCpf());
				cliente.setNome(clienteDto.getNome());
				if (StringUtils.isNotEmpty(clienteDto.getDataNascimento())) {
					cliente.setDataNascimento(DataUtil.converteStringDataHora(clienteDto.getDataNascimento()));
				}
				return salvaCliente(cliente);
			}
		}
		return null;
	}

}
