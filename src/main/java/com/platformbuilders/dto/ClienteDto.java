package com.platformbuilders.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"nome", "cpf", "dataNascimento"}, 
	callSuper =  false)
@ApiModel(description = "Classe utilizada para conversão da temperatura")
public class ClienteDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Nome do Cliente", example = "Pedro Botelho", required = true, position = 0)
	private String nome;
	
	@ApiModelProperty(notes = "CPF", example = "78956745345", required = true, position = 1)
	private String cpf;
	
	@ApiModelProperty(notes = "Data de Nascimento", example = "22/01/1985", required = true, position = 2)
	private String dataNascimento;
	
	@ApiModelProperty(notes = "Idade", example = "32", required = true, position = 2)
	private int idade;

	public ClienteDto(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}

	public ClienteDto(String nome, String cpf, String dataNascimento) {
		this.nome = nome;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}
}
