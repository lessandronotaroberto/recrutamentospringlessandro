package com.platformbuilders.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 
@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"nome", "cpf"}, 
	callSuper =  false)
@ApiModel(description = "Entidade de histórico das conversões realizadas.")
public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_cliente", nullable = false)
	private Long id;
	
    @Column(name = "nome", nullable = false, length = 255)
	private String nome;

    @Column(name = "cpf", nullable = false, length = 11)
	private String cpf;
	
    @Column(name = "dt_nascimento", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascimento;

	public Cliente(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}

}
