package com.platformbuilders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.platformbuilders.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	@Query(value = "SELECT c FROM Cliente c where lower(c.nome) = :nome or c.cpf = :cpf") 
    Cliente buscaPorNomeCpf(@Param("nome") String nome, @Param("cpf") String cpf);
	
	@Query(value = "SELECT c FROM Cliente c where c.cpf = :cpf") 
    Cliente buscaPorCpf(@Param("cpf") String cpf);

	@Query(value = "delete from Cliente c where c.cpf = :cpf")
	void removePorCpf(@Param("cpf") String cpf);

}
