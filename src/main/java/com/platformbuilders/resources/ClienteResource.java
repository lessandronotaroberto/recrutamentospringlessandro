package com.platformbuilders.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platformbuilders.dto.ClienteDto;
import com.platformbuilders.model.Cliente;
import com.platformbuilders.resources.exception.ValidationException;
import com.platformbuilders.service.ClienteService;
import com.platformbuilders.util.MessageUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "Recurso que exibe os dados dos clientes.")
public class ClienteResource {

	private static final Logger logger = LoggerFactory.getLogger(ClienteResource.class);

	private ClienteService clienteService;
	public MessageUtil messageUtil;

	@Autowired
	public ClienteResource(ClienteService clienteService, MessageUtil messageUtil) {
		this.clienteService = clienteService;
		this.messageUtil = messageUtil;
	}

	@ApiOperation(value = "Chamada que retorna os clientes paginados")
	@RequestMapping(value = "/clientes", method = RequestMethod.GET)
	public ResponseEntity<Page<ClienteDto>> buscaClientes(
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "dataInclusao") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Cliente> list = clienteService.findPage(page, linesPerPage, orderBy, direction);
		Page<ClienteDto> listDto = list.map(obj -> {
			try {
				return clienteService.preparaClienteDto(obj);
			} catch (ValidationException e) {
				logger.error(e.getMessage());
			}
			return null;
		});
		return ResponseEntity.ok().body(listDto);
	}

	@ApiOperation(value = "Busca o cliente")
	@RequestMapping(value = "/cliente", method = RequestMethod.GET)
	public ResponseEntity<?> buscaCliente(@ApiParam("Informações do cliente.") @RequestBody ClienteDto clienteDto) {
		try {
			ClienteDto clienteDtoDB = clienteService.buscaPorNomeCpf(clienteDto);
			return ResponseEntity.ok().body(clienteDtoDB);
		} catch (ValidationException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<String>(
					messageUtil.getMessage("msg.clienteNaoEncontrado", clienteDto.getCpf(), clienteDto.getNome()),
					HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(messageUtil.getMessage("msg.excecao.carregamento.cliente.nome.cpf",
					clienteDto.getCpf(), clienteDto.getNome()));
		}

	}

	@PostMapping(value = "/salvarCliente", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> salvaCliente(@ApiParam("Informações do cliente.") @RequestBody ClienteDto clienteDto) {
		try {
			Cliente cliente = clienteService.insert(clienteDto);
			return ResponseEntity.ok().body(cliente);
		} catch (ValidationException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(messageUtil.getMessage("msg.excecao.gravacao.cliente"));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value = "/clientes/{cpf}")
	public ResponseEntity<?> removeCliente(@PathVariable String cpf) {
		try {
			if (clienteService.removePorCpf(cpf)) {
				return ResponseEntity.ok(messageUtil.getMessage("msg.cliente.remocao.sucesso", cpf));
			}
			return new ResponseEntity(messageUtil.getMessage("msg.clienteNaoEncontrado", cpf), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(messageUtil.getMessage("msg.excecao.remocao.cliente"));
		}
	}

	@PatchMapping("/clientes/{id}")
	public ResponseEntity<?> atualizaPatchCliente(@RequestBody ClienteDto clienteDto,
			@PathVariable("id") String id) {
		try {
			Cliente cliente = clienteService.atualizarCliente(clienteDto, id);
			return new ResponseEntity<>(cliente, HttpStatus.OK);
		} catch (ValidationException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());	  
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(messageUtil.getMessage("msg.excecao.atualizacao.cliente"));
		}
	}

	@PutMapping("/clientes/{id}")
	public ResponseEntity<?> alteraCliente(@RequestBody ClienteDto clienteDto,
			@PathVariable("id") String id) {
		try {
			Cliente cliente = clienteService.atualizarCliente(clienteDto, id);
			return new ResponseEntity<>(cliente, HttpStatus.OK);
		} catch (ValidationException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());	  
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(messageUtil.getMessage("msg.excecao.atualizacao.cliente"));
		}
	}
}
